CONTAINER=mlaopane.rxhr
IMAGE=node:12
RUN_CMD=docker exec -it $(CONTAINER)

# Shortcuts
clean : packages_uninstall container_stop
enter : container_enter
init  : container_remove container_run
start : container_start
stop  : container_stop
test  : tests_run

# Longcuts
container_enter:	
	$(RUN_CMD) bash

container_remove:
	docker rm -f $(CONTAINER) || true

container_run:
	docker run \
		-dit \
		--name $(CONTAINER) \
		--volume $$(pwd):/app \
		--workdir /app \
		$(IMAGE) || true

container_start:
	docker start $(CONTAINER)

container_stop:
	docker stop $(CONTAINER)

packages_install:
	$(RUN_CMD) yarn install

packages_uninstall:
	$(RUN_CMD) rm -rf node_modules

tests_run:
	$(RUN_CMD) yarn test
