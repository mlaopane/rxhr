type RequestMethod = 'GET' | 'POST' | 'DELETE' | 'PUT' | 'PATCH';

export interface RequestHeaders {
    [key: string]: string;
}

export interface RequestOptions {
    headers?: RequestHeaders;
    method: RequestMethod;
    payload?: any;
    url: string;
}

export type RequestOptionsWithPayload<
    Method extends RequestMethod
> = RequestOptions & {
    method: Method;
    payload: any;
};

export type ObservableResponse<Body> = {
    statusCode: number;
    body: Body;
};
