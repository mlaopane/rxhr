import { Observer, Observable } from 'rxjs';
import { RequestOptions, ObservableResponse } from '@src/types';
import { createRequest } from './createRequest';

export function sendRequest<ResponseBody = any>({
    url,
    method = 'GET',
    headers = {},
    payload,
}: RequestOptions) {
    const request = createRequest();

    const subscriber = (
        observer: Observer<ObservableResponse<ResponseBody>>,
    ) => {
        request.initialize({ headers, method, observer, url });

        if (!payload) {
            request.sendWithoutPayload();
        } else {
            request.sendWithPayload(payload);
        }

        return function unsubscribe() {
            request.abort();
        };
    };

    return new Observable(subscriber);
}
