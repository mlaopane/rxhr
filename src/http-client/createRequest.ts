import { onReadyStateChange } from './onReadyStateChange';
import { setRequestHeaders } from './setHeaders';

export function createRequest() {
    const xhr = new XMLHttpRequest();

    function setObserver<ResponseBody>(observer) {
        xhr.addEventListener(
            'readystatechange',
            onReadyStateChange<ResponseBody>({ observer, xhr }),
        );
    }

    return {
        abort() {
            xhr.abort();
        },

        initialize({ headers, method, observer, url }) {
            setObserver(observer);
            setRequestHeaders({ headers, xhr });
            xhr.open(method, url);
        },

        sendWithPayload(payload) {
            if (payload instanceof FormData) {
                xhr.send(payload);
            } else {
                xhr.send(JSON.stringify(payload));
            }
        },

        sendWithoutPayload() {
            xhr.send();
        },
    };
}
