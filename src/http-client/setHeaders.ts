import { RequestHeaders } from '@src/types';

interface SetRequestHeadersProps {
    xhr: XMLHttpRequest;
    headers: RequestHeaders;
}

function setRequestHeader(xhr: XMLHttpRequest) {
    return function([key, value]: [string, string]) {
        xhr.setRequestHeader(key, value);
    };
}

export function setRequestHeaders({ xhr, headers }: SetRequestHeadersProps) {
    Object.entries(headers).forEach(setRequestHeader(xhr));
}
