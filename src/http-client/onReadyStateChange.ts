import { Observer } from 'rxjs';

interface OnReadyStateChangeProps<T> {
    xhr: XMLHttpRequest;
    observer: Observer<{ statusCode: number; body: T }>;
}

export function onReadyStateChange<T>({
    xhr,
    observer,
}: OnReadyStateChangeProps<T>) {
    return function() {
        if (xhr.readyState !== xhr.DONE) {
            return;
        }

        const { response: body, status: statusCode } = xhr;

        if (statusCode >= 400) {
            console.error(body);
            if (observer.error) {
                observer.error({ body, statusCode });
            }
        } else {
            const contentType =
                xhr.getResponseHeader('content-type') || 'text/plain';

            if (contentType.includes('json')) {
                observer.next({ statusCode, body: JSON.parse(body) });
            } else {
                observer.next({ statusCode, body });
            }
        }

        if (observer.complete) {
            observer.complete();
        }
    };
}
