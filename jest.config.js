module.exports = {
    collectCoverageFrom: ['src/*.{ts}'],
    moduleDirectories: ['node_modules', 'src'],
    moduleNameMapper: {
        '@src/(.*)$': '<rootDir>/src/$1',
    },
    preset: 'ts-jest',
    rootDir: '.',
    testMatch: ['**/tests/**/?(*.)+(spec|test).ts'],
    verbose: true,
};
