import HttpClient from '@src/http-client';

interface Todo {
    id: number;
}

describe(`Sending an HTTP request`, function() {
    it(`should work`, function(done) {
        expect.assertions(2);

        HttpClient.sendRequest<Todo[]>({
            method: 'GET',
            url: `https://jsonplaceholder.typicode.com/todos/1`,
        }).subscribe({
            next: ({ body, statusCode }) => {
                expect(statusCode).toBe(200);
                expect(body).toMatchObject({
                    id: 1,
                });
            },
            complete: () => {
                done();
            },
        });
    });
});
