import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';
import globals from 'rollup-plugin-node-globals';
import resolve from 'rollup-plugin-node-resolve';
import pkg from './package.json';

const extensions = ['.js', '.ts', '.json'];

export default {
    input: 'src/http-client.ts',
    output: [
        {
            file: pkg.main,
            format: 'cjs',
            exports: 'named',
            sourcemap: true,
        },
        {
            file: pkg.module,
            format: 'es',
            exports: 'named',
            sourcemap: true,
        },
    ],
    plugins: [
        babel({
            exclude: 'node_modules/**',
            extensions,
            runtimeHelpers: true,
        }),
        commonjs(),
        globals(),
        resolve({ extensions }),
    ],
};
